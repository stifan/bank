<?php

class OverviewController extends Controller {
    public function __construct() {
        parent::__construct();

        //make sure that the user is logged in
        $this->validateUser();
    }

    public function index() {
        $loggedInUser = $this->session->getLoggedInUser();
        if($loggedInUser === null) {
            $this->redirect("login/");
        }

        // get users accounts
        $accountModel = new AccountModel();
        $userAccounts = $accountModel->getUserAccounts($loggedInUser);

        $contentView = new View();
        $contentView->assign("accounts", $userAccounts);

        $contentView->render("overview");
    }
}