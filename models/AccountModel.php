<?php

class AccountModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function getUserAccounts($userId) {
        $sql = "SELECT *
                FROM `account`
                WHERE userId = ?";

        $accounts = $this->db->query($sql, array($userId));

        if($accounts) {
            return $accounts;
        }

        return array();
    }

    public function getAccount($accountId) {
        $sql = "SELECT *
                FROM `account`
                WHERE id = ?
                LIMIT 1";

        $account = $this->db->query($sql, array($accountId));

        return $this->getSingleResult($account);
    }

    public function getAccountWithTransfers($accountId) {
        $account = $this->getAccount($accountId);

        if($account) {
            $transferModel = new TransferModel();
            $transfers = $transferModel->getTransfersFromAccount($accountId);
            $account->transfers = array();
            if($transfers) {
                $account->transfers = TransferModel::formatTransfers($transfers, $account->id);
            }
        }

        return $account;
    }

    public function insertAccount($name, $userId) {
        $sql = "INSERT INTO `account`
                (`userId`, `name`, `balance`)
                VALUES(?, ?, 1000);";

        $result = $this->db->query($sql, array($userId, $name));

        return $result;
    }
} 