<?php

class UserController extends Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $view = new View();
        $view->assign("title", "Edit User");

        $view->render("edit_user");
    }

    public function edit($userID, $token) {

        var_dump($userID, $token);

        $view = new View();
        $view->assign("title", "Edit user");

        $view->render("edit_user");
    }

    public function signup() {
        $content_view = new View();
        $content_view->render("signup");
    }

    public function Register()
    {
        //Sanitizes all the inputs
        $firstname = Validator::sanitizeInput($_POST['firstname']);
        $lastname = Validator::sanitizeInput($_POST['lastname']);
        $email = Validator::sanitizeInput($_POST['email']);
        $password = Validator::sanitizeInput($_POST['password']);
        $confirm_password = Validator::sanitizeInput($_POST['confirm_password']);
        $month = Validator::sanitizeInput($_POST['month']);
        $day = Validator::sanitizeInput($_POST['day']);
        $year = Validator::sanitizeInput($_POST['year']);
        $gender = Validator::sanitizeInput($_POST['gender']);

        if(is_null(Validator::isEmail($email))) {
            $this->redirect("user/signup/");
            return;
        }

        if($password == $confirm_password){
            $birthday = Validator::prepareBirtdayFormat($year, $month, $day);
            $userModel = new UserModel();
            $result = $userModel->Register($firstname, $lastname, $email, $password, $birthday, $gender);
            //If user is registered, create the keyset for user
            if($result){
                $keySet = new KeySetModel($userModel->db->insert_id);
                $keySet->createSet();
                $imagePath = $keySet->createKeySetImage();

                $this->redirect("/index/");
            }else{
                $this->redirect("/user/signup/");
            }
        } else {
            $this->redirect("user/signup/");
            return;
        }
    }

    public function create() {




    }
}