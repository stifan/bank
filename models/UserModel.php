<?php
/**
 * Created by PhpStorm.
 * User: steffen
 * Date: 07-01-14
 * Time: 15:33
 */

class UserModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    public static function checkLogin($email, $password) {
        $db = new Database();
        $sql = "SELECT * FROM `user`
                WHERE `email` = ?";
        $user = $db->query($sql, array($email));
        if($user) {
            // if the user is an array and there is only one iteim in the array
            // then shift the first item of the array or return null
            $user = (is_array($user) && count($user) === 1) ? array_shift($user) : null;
            if(is_null($user)) {
                return null;
            }

            if(Hasher::valueIsValid($password, $user->passwordHash)) {
                return $user->id;
            }
        } else {
            return null;
        }
    }

    public function Register($firstname, $lastname, $email, $password, $birthday, $gender){
        $array = array($email);
        $sqlQuery="SELECT * FROM user WHERE email = ?";

        $seeIfUserExist = $this->db->query($sqlQuery, $array);

        if($seeIfUserExist) {
            return null;
        }else{
            $hashedPassword = Hasher::hashValue($password);
            $insertArray = array($firstname, $lastname, $email, $hashedPassword, $birthday, $gender);
            $sQuery =  "INSERT INTO user (firstname, lastname, email, passwordHash, birthday, gender) VALUES (?,?,?,?,?,?)";
            return $this->db->query($sQuery, $insertArray);
        }
    }
} 