<?php

class EasyIdController extends Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        // make sure that the user has successfully authenticated.
        if(!$this->session->get("loginUserId")) {
            $this->redirect("/Login/");
        }

        // Get a random unused keyset for validation.
        $keySetModel = new KeySetModel($this->session->get("loginUserId"));
        $keySet = $keySetModel->getActiveKeySet();

        $contentView = new View();

        $contentView->assign("keySet", $keySet);

        $contentView->render("easyid");
    }

    public function validateId() {
        // make sure that the user has succesfully logged in.
        $userId = $this->session->get("loginUserId");
        if(is_null($userId)) {
            $this->redirect("/Login/");
        }

        $id = $_POST['keySetId'];
        $key = $_POST['key'];

        $keySetModel = new KeySetModel($userId);
        $keySetId = $keySetModel->validateKeySet($id, $key);
        if(!is_null($keySetId)) {
            // Keyset is valid, set keyset to inactive
            $keySetModel->deactivateKeySet($keySetId);
            // Log user in
            $this->session->setLoggedInUser($userId);
            // Redirect to his overview
            $this->redirect("/Overview/");
        } else {
            // Redirect back to easy id page with wrong password error.
        }
    }
}