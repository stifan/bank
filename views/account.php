<div class="container">
            <div class="overview-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 col-md-3">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-folder-close">
                                            </span>Personal finances</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <span class="glyphicon glyphicon-th-list text-primary"></span>
                                                        <a href="/overview/">Accounts</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="glyphicon glyphicon-plus text-primary"></span>
                                                        <a href="/account/create">Create Account</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-th">
                                            </span>Payments and transfers</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <span class="glyphicon glyphicon-transfer text-primary"></span>
                                                        <a href="/transfer/">Transfer</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-user">
                                            </span>Account</a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <span class="glyphicon glyphicon-off text-danger"></span>
                                                        <a href="/login/logout/" class="text-danger">Log out</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-9">
                            <div class="well">
                               <div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-success" style="display:none;"></div>
                                            <h4>Account: <?php echo $data['account']->name . " - " . $data['account']->id; ?></h4>
                                            <h4>Amount: <?php echo $data['account']->balance; ?> $</h4>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Transaction
                                                        </th>
                                                        <th>
                                                            Date
                                                        </th>
                                                        <th>
                                                            Amount
                                                        </th>
                                                        <th>
                                                            Comment
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(isset($data['account'])) {
                                                    foreach($data['account']->transfers as $transfer) {
                                                        $rowClass = ($transfer->amount > 0) ? "success" : "danger";
                                                        ?>
                                                        <tr class="<?php echo $rowClass; ?>">
                                                            <td><?php echo $transfer->note; ?></td>
                                                            <td><?php echo date('j F Y',strtotime($transfer->date)); ?></td>
                                                            <td><?php echo $transfer->amount; ?> $</td>
                                                            <td><?php echo $transfer->comment; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->