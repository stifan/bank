<?php
/**
 * Created by PhpStorm.
 * User: steffen
 * Date: 08-01-14
 * Time: 16:45
 */

class AccountController extends Controller {

    public function __construct() {
        parent::__construct();

        // Make sure that the user is logged in when using the controller
        $this->validateUser();
    }

    public function show($accountId) {
        $this->model = new AccountModel();
        $this->view->assign("account", $this->model->getAccountWithTransfers($accountId));
        $this->view->render("account");
    }

    public function create() {
        // Make request token
        $this->view->assign("token", Token::generate());
        $this->view->render("createAccount");
    }

    public function createAccount() {
        // validate token
        $session = new Session();
        var_dump($_POST['token'], $session->get('token'));
        if(Token::check($_POST['token'])) {
            $accountName = Validator::sanitizeInput($_POST['name']);
            $accountModel = new AccountModel();
            if($accountModel->insertAccount($accountName, $this->session->getLoggedInUser())) {
                $this->redirect("/overview/");
            } else {
                $this->redirect("/account/create/");
            }
        } else {
            die("Invalid token, you BASTARD!");
        }
    }
} 