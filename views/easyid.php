<div class="container">
    <div class="starter-template">
        <div class="container" id="wrap">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <?php if(isset($data['keySet'])) { ?>
                    <form action="/EasyId/validateId" method="post" accept-charset="utf-8" class="form form-horizontal" role="form">   <legend>Easy Id</legend>
                        <p>Enter the key that matches the Id beneath</p>
                        <input type="text" disabled name="" value="<?php echo $data['keySet']->keySetId; ?>" class="form-control input-lg" required />
                        <input type="text" name="key" value="" class="form-control input-lg" placeholder="Key" required />
                        <br />
                        <input type="hidden" name="keySetId" value="<?php echo $data['keySet']->keySetId; ?>" />
                        <button class="btn btn-lg btn-primary btn-block signup-btn" type="submit">Login</button>
                    </form>
                    <?php } else { ?>
                        <p>There was an error, go back and try to login again!</p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
