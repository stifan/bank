<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Validator
 *
 * @author Morten
 */
class Validator {
    public static function sanitizeInput($input){
        return htmlentities($input);
    }
    
    public static function isNumeric($input){
        if(is_numeric($input)) {
            return self::sanitizeInput($input);
        } else {
            return null;
        }
    }

    public static function isEmail($str) {
        //Check to see if email syntax is valid.
        if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$str))
        {
            return null;
        }
        return $str;
    }

    /**
     * Prepares the date format to match the format found in the DB. (YYYY-MM-DD)
     *
     * @param $year
     * @param $day
     * @param $month
     * @return string
     */
    public static function prepareBirtdayFormat($year, $day, $month)
    {
        if($month < 10){
            $month = '0'.$month;
        }else if($month >= 10){
            $month;
        }

        if($day < 10){
            $day = '0'.$day;
        }else if($day >= 10){
            $day;
        }
        return $result = $year.'-'.$month.'-'.$day;
    }
}

