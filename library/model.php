<?php

class Model {

    public $db;

    function __construct() {
        $this->db = new Database();
    }

    /**
     * Will take a single result in an array and return only the single result.
     *
     * @param $result
     * @return array|mixed|null
     */
    protected function getSingleResult($result) {
        $result = (is_array($result) && count($result) === 1) ? array_shift($result) : null;
        if(is_null($result)) {
            return null;
        }
        return $result;
    }
}