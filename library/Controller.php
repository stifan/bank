<?php

class Controller {
    protected $view;
    protected $model;
    protected $view_name;
    protected $session;
    protected $token;

    public function __construct() {
        $this->view_name = '';
        $this->view = new View();
        $this->session = new Session();
    }

    public function assign($var, $val) {
        $this->view->assign($var, $val);
    }

    public function loadModel($name) {
        $modelName = $name . "Model";
        $this->model = new $modelName();
    }

    public function loadView($name) {
        $viewPath = ROOT . DS . "views" . DS . strtolower($name) . ".php";
            if(file_exists($viewPath)) {
                $this->view_name = $name;
        }
    }

    public function redirect($url) {
        // check if user forgot to include / at the start
        if(strpos($url, "/") !== 0) {
            $url = "/" . $url;
        }

        // sending the header redirect
        if(!headers_sent()) {
            header("Location: " . SITE_ROOT . $url, true);
        }
    }

    /**
     * Validates if the user is logged in. If not
     * the user is redirected to login screen
     */
    public function validateUser() {
        $loggedInUser = $this->session->getLoggedInUser();
        if($loggedInUser === null) {
            $this->redirect("login/");
        }
        return true;
    }

    public function __destruct() {
        if(!empty($this->view_name)) {
            $this->view->render($this->view_name);
        }
    }
}