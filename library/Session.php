<?php
/**
 * Created by PhpStorm.
 * User: steffen
 * Date: 08-01-14
 * Time: 10:18
 */

class Session {

    public function __construct() {
        $cur_session = session_id();
        if(empty($cur_session)) {
            session_start();
        }
    }

    public function get($name) {
        if(isset($_SESSION['data'][$name])) {
            return $_SESSION['data'][$name];
        }

        return null;
    }

    public function set($name, $value) {
        $_SESSION['data'][$name] = $value;
        return true;
    }

    /**
     * Returns the id of the current logged in user
     *
     * @return int|null
     */
    public function getLoggedInUser() {
        if(!isset($_SESSION['auth']) || !isset($_SESSION['auth']['userId'])) {
            return null;
        }

        return $_SESSION['auth']['userId'];
    }

    /**
     * Set the user id of the current logged in user
     *
     * @param $userId
     */
    public function setLoggedInUser($userId) {
        $_SESSION['auth'] = array('userId' => $userId);
        $this->regen();
    }

    public function destroy() {
        $_SESSION = array();
        session_destroy();
    }

    public function sessionUnset($name) {
        if(isset($_SESSION['data'][$name])) {
            unset($_SESSION['data'][$name]);
            return true;
        }
        return false;
    }

    public function regen() {
        session_regenerate_id();
    }
} 