        <div class="container">
            <div class="overview-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 col-md-3">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-folder-close">
                                            </span>Personal finances</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <span class="glyphicon glyphicon-th-list text-primary"></span>
                                                        <a href="/overview/">Accounts</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="glyphicon glyphicon-plus text-primary"></span>
                                                        <a href="/account/create">Create Account</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-th">
                                            </span>Payments and transfers</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <span class="glyphicon glyphicon-transfer text-primary"></span>
                                                        <a href="/transfer/">Transfer</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-user">
                                            </span>Account</a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <span class="glyphicon glyphicon-off text-danger"></span>
                                                        <a href="/login/logout/" class="text-danger">Log out</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-9">
                            <div class="well">
                               <div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-success" style="display:none;"></div>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Account Name
                                                        </th>
                                                        <th>
                                                            Account Number
                                                        </th>
                                                        <th>
                                                            Last Transaction
                                                        </th>
                                                        <th>
                                                            Balance
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(isset($data['accounts'])) {
                                                    foreach($data['accounts'] as $account) {
                                                        $rowClass = ($account->balance > 0) ? "success" : (($account->balance == 0) ? "warning" : "danger");
                                                        ?>
                                                        <tr class="<?php echo $rowClass; ?>">
                                                            <td><a href="<?php echo SITE_ROOT . "/account/show/" . $account->id; ?>"><?php echo $account->name; ?></a></td>
                                                            <td><?php echo $account->id; ?></td>
                                                            <td></td>
                                                            <td><?php echo $account->balance; ?> $</td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->