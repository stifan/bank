<?php

// Application vars
define("DEV_ENVIRONMENT", true);
define("SITE_ROOT", "http://bank.localhost:3333");
define("ROOT_DIRECTORY", $_SERVER['DOCUMENT_ROOT']);
define("DEFAULT_CONTROLLER", "index");
define("DEFAULT_ACTION", "index");

// Database vars
define("DB_HOST", "localhost");
define("DB_NAME", "bank");
define("DB_USER", "root");
define("DB_PASSWORD", "");
