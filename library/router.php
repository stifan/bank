<?php

/**
 * Check if dev mode is on and display errors
 */
function setReporting()  {
    if(DEV_ENVIRONMENT) {
        error_reporting(E_ALL);
        ini_set('display_errors', 'On');
    } else {
        error_reporting(E_ALL);
        ini_set('display_errors', 'Off');
        init_set('log_errors', 'On');
        init_set('error_log', ROOT . DS . 'tmp' . DS . 'logs' . DS . 'error.log');
    }
}

/**
 * Take a param that can either be an array or not
 *
 * if it is an array, map the array using itself
 * else, just strip the slashes
 *
 * @param $value
 * @return array|string
 */
function stripSlashesDeep($value) {
    return is_array($value) ? array_map('stripSlashesDeep', $value) : stripcslashes($value);
}

/**
 * Strip slashes from super globals GET, POST, COOKIE
 */
function removeMagicQuotes() {
    if(get_magic_quotes_gpc()) {
        $_GET = stripSlashesDeep($_GET);
        $_POST = stripSlashesDeep($_POST);
        $_COOKIE = stripSlashesDeep($_COOKIE);
    }
}

function unregisterGlobals() {
    if(ini_get('register_globals')) {
        $globalsArray = array(
            "_SESSION",
            "_POST",
            "_GET",
            "_COOKIE",
            "_REQUEST",
            "_SERVER",
            "_ENV",
            "_FILES"
        );

        foreach($globalsArray as $global) {
            foreach($GLOBALS[$global] as $key => $val) {
                if($val === $GLOBALS[$key]) {
                    unset($GLOBALS[$key]);
                }
            }
        }
    }
}

/**
 * Automatically include the files containing
 * the classes that are called
 *
 * @param $className
 */
function __autoload($className) {
    $folders = array("controllers", "models", "library");
    $classFound = false;
    foreach($folders as $folder) {
        $path = ROOT . DS . $folder . DS . strtolower($className) . ".php";
        if(file_exists($path)) {
            $classFound = true;
            require_once($path);
            break;
        }
    }

    if(!$classFound) {
        die("Error: Class " . $className . " was not found");
    }

}

function callHook() {
    global $url;
    if(!isset($url)) {
        $controllerName = DEFAULT_CONTROLLER;
        $action = DEFAULT_ACTION;
    } else {
        $urlArray = array();
        $urlArray = explode("/", $url);
        $controllerName = $urlArray[0];
        $action = (isset($urlArray[1]) && $urlArray[1] != '') ? $urlArray[1] : DEFAULT_ACTION;
    }

    $query1 = (isset($urlArray[2]) && $urlArray[2] != '') ? $urlArray[2] : null;
    $query2 = (isset($urlArray[3]) && $urlArray[3] != '') ? $urlArray[3] : null;

    // modify controller name to fit naming
    $class = ucfirst($controllerName) . "Controller";

    // instantiate the appropriate class
    if(class_exists($class) && (int)method_exists($class, $action)) {
        $controller = new $class;
        $controller->$action($query1, $query2);
    } else {
        // Error: Controller class not found
        die("1. File <strong>'$controllerName.php'</strong> containing class <strong>'$class'</strong> might be missing. 2. Method <strong>'$action'</strong> is missing in <strong>'$controllerName.php'</strong>");
    }
}

setReporting();
removeMagicQuotes();
unregisterGlobals();
callHook();
