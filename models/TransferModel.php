<?php

class TransferModel extends Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function transfer($amount, $note, $senderAccount, $receiverAccount, $comment){

        try {
            $this->db->beginTransaction();

            $transferMainSql = "INSERT INTO transfer VALUES (DEFAULT, ?, ?, ?, ?, ?, DEFAULT)";
            $transferStmt = $this->db->prepareStatement($transferMainSql);
            $args = array($amount, $note, $senderAccount, $receiverAccount, $comment);
            $transferStmt->execute($args);

            $senderTransactionSql = "UPDATE `account`
                                    SET `balance` = `balance` - ?
                                    WHERE id = ?";
            $senderTransStmt = $this->db->prepareStatement($senderTransactionSql);
            $args = array($amount, $senderAccount);
            $senderTransStmt->execute($args);

            $receiverTransactionSql = "UPDATE `account`
                                    SET `balance` = `balance` + ?
                                    WHERE id = ?";
            $receiverTransStmt = $this->db->prepareStatement($receiverTransactionSql);
            $args = array($amount, $receiverAccount);
            $receiverTransStmt->execute($args);

            $this->db->commitTransaction();

            return true;
        } catch (Exception $e) {
            if(isset($this->db)) {
                $this->db->rollBackTransaction();
                die("Error with transfer try again!");
                return false;
            }
        }
    }

    public function getTransfersFromAccount($accountId) {
        $sql = "SELECT * FROM `transfer`
                WHERE `senderAccountId` = ?
                OR `receiverAccountId` = ?
                ORDER BY `date` ASC";

        $args = array($accountId, $accountId);

        return $this->db->query($sql, $args);
    }

    /**
     * Will set negative values for transfers being made
     * by the active account.
     *
     * @param $transfers
     * @param $accountId
     * @return mixed
     */
    public static function formatTransfers($transfers, $accountId) {
                foreach($transfers as $transfer) {
            if((int) $transfer->senderAccountId === (int) $accountId) {
                $transfer->amount = "-" . $transfer->amount;
            }
        }

        return $transfers;
    }

}
