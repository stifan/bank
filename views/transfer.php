<div class="container">
    <div class="paymentform">
        <div class="row">
            <div class='col-md-4'></div>
            <div class='col-md-4'><h1>Transfer</h1></div>
            <div class='col-md-4'></div>
        </div>

        <div class='row'>
        <div class='col-md-4'></div>
        <div class='col-md-4 greybackground'>

          <form action="/transfer/transfer" class="require-validation" id="transfer-form" method="post">
            <div class='form-row'>
                <div class='col-xs-12 form-group required'>
                    <label class='control-label'>Note for transfer</label>
                    <input name="note" class='form-control' size='4' type='text'>
                </div>
                <div class='col-xs-12 form-group required'>
                    <label class='control-label'>Amount $</label>
                    <input name="amount" class='form-control' size='4' type='text'>
                </div>
                <div class='col-xs-12 form-group required'>
                <label class='control-label'>Choose account</label>
                <select name="senderAccount">
                    <?php foreach ($data['accounts'] as $value) {
                        echo '<option value="'.$value->id.'">'.$value->name.'</option>';
                    } ?>
                </select>
                </div>
                    <div class='col-xs-12 form-group required'>
                    <label class='control-label'>Receivers account number</label>
                    <input name="receiverAccount" class='form-control' size='4' type='text'>
                </div>
                <div class='col-xs-12 form-group required'>
                    <label class='control-label'>Comment for receiver</label>
                    <input name="comment" class='form-control' size='4' type='text'>
                    <input type="hidden" name="token" value="<?php echo $data['token']; ?>" />
                </div>
            </div>
            <div class='form-row'>
              <div class='col-md-12 form-group'>
                <button class='form-control btn btn-success submit-button total' type='submit'>Transfer</button>
              </div>
            </div>
          </form>
        </div>
        <div class='col-md-4'></div>
    </div>
    </div>
</div>