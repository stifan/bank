<?php

class Database extends PDO {

    protected $PDO;
    public $affectedRows;
    public $insert_id;

    function __construct() {
        // call the PDO constructor
        try {
            $this->PDO = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8", DB_USER, DB_PASSWORD);
        } catch(PDOException $e) {
            die("Could not connect to database: " . $e->getMessage());
        }
    }

    /**
     * Execute query on Database
     *
     * Returns all rows as array of objects or true if query succeeded
     * but no results where found.
     *
     * @param string $query
     * @param null $data
     * @return bool|PDOStatement
     */
    public function query($query, $data = null) {

        try {
            // Bind query first
            if($stmt = $this->PDO->prepare($query)) {

                // Succesfully binded the query, now execute with data arrary
                $result = $stmt->execute($data);

                if($result) {
                    // There is a result, check if there is no rows returned
                    if(!$stmt->fetch(PDO::FETCH_NUM)) {
                        // No results
                        if($stmt->rowCount() > 0) {
                            if( (int)$this->PDO->lastInsertId() > 0) {
                                $this->insert_id = (int)$this->PDO->lastInsertId();
                            }
                            $this->affectedRows = $stmt->rowCount();
                            return true;
                        }
                        return false;
                    }

                    // Get all returned rows
                    $results = array();
                    $stmt->execute($data);
                    while($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                        $results[] = $row;
                    }
                    return $results;

                } else {
                    $error = $stmt->errorInfo();
                    echo "Query failed with message: " . $error[2];
                }
            }
        } catch (PDOException $e) {
            echo "A database problem has occurred: " . $e->getMessage();
        }
    }

    public function beginTransaction() {
        $this->PDO->beginTransaction();
        return;
    }

    public function commitTransaction() {
        $this->PDO->commit();
        return;
    }

    public function rollBackTransaction() {
        $this->PDO->rollBack();
        return;
    }

    public function prepareStatement($stmt) {
        return $this->PDO->prepare($stmt);

    }

    public function getLastInsertedId() {
        return $this->PDO->lastInsertId();
    }
}