<?php

class LoginController extends Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $content_view = new View();
        $content_view->render("login");
    }

    public function login() {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $user = UserModel::checkLogin($email, $password);

        if(!is_null($user)) {
            $this->session->set("loginUserId", (int)$user);
            $this->redirect("/EasyId/");
        } else {
            $this->redirect("/Login/");
        }
    }

    public function logout() {
        $this->session->destroy();
        $this->redirect("/");
    }
}