<?php

class KeySetModel extends Model {

    private $keySetId;
    private $keySet;
    private $userId;
    private static $ITEMS_IN_SET = 87;
    private static $KEY_LENGTH = 6;


    function __construct($userId) {
        parent::__construct();

        $this->userId = $userId;
    }

    public function createSet() {
        $this->keySet = array();
        $tempItemId = 0;
        for($i = 0; $i < self::$ITEMS_IN_SET; $i++) {
            $set = new stdClass();

            $set->id = $tempItemId = $this->generateId($tempItemId);
            $set->key = $this->generateKey();
            $set->keyHash = Hasher::hashValue($set->key);

            array_push($this->keySet, $set);
        }

        $this->saveKeySet();
    }


    private function generateId($tempItemId) {
        // get a random number between 1 and 15
        $newId = $tempItemId+rand(1, 25);


        return "" . $this->formatId($newId);
    }

    private function formatId($id) {
        $missingDigits = 4 - strlen($id);

        for($i = 0; $i < $missingDigits; $i++) {
            $id = "0" . $id;
        }

        return $id;
    }

    private function generateKey() {
        $keyDigits = "abcdef0123456789";
        $key = null;

        while(!$this->keyIsValid($key)) {
            for($i = 0; $i < self::$KEY_LENGTH; $i++) {
                $key .= $keyDigits[mt_rand(0, (strlen($keyDigits)-1))];
            }
        }

        return $key;
    }

    private function keyIsValid($key = null) {

        // return false if the key is null
        if(is_null($key)) {
            return false;
        }
        // make sure that the key is unique in the existing set
        foreach($this->keySet as $item) {
            if($key == $item->key) {
                return false;
            }
        }
        return true;
    }

    private function saveKeySet() {
        try {
            $this->db->beginTransaction();

            $easyIdSql = "INSERT INTO `easyid`
                          (`userId`) VALUES (?);";

            $easyIdStmt = $this->db->prepareStatement($easyIdSql);
            $easyIdStmt->execute(array($this->userId));

            $this->keySetId = $this->db->getLastInsertedId();
            $keySetSql = "INSERT INTO `easyidkeyset`
                          (`easyIdId`, `keySetId`,`keyHash`)
                          VALUES ";
            for($i = 0; $i < self::$ITEMS_IN_SET; $i++) {
                $keySet = $this->keySet[$i];
                $keySetSql .= "(".$this->keySetId.", ".$keySet->id.",'".$keySet->keyHash."')";
                if($i+1 != self::$ITEMS_IN_SET) {
                    $keySetSql .= ", ";
                }
            }
            $keySetSql .= ";";

            $keySetStmt = $this->db->prepareStatement($keySetSql);
            $keySetStmt->execute();

            $this->db->commitTransaction();
            return true;

        } catch (Exception $e) {
            $this->db->rollBackTransaction();
            return false;
        }
    }


    public function createKeySetImage() {

        $width = 600;
        $height = 800;

        // Create the image
        $im = imagecreatetruecolor($width, $height);

        // Create colors for the image
        $black = imagecolorallocate($im, 255, 255, 255);
        $grey = imagecolorallocate($im, 150, 150, 150);

        // fill background with grey
        imagefill($im, 0, 0, $grey);

        // The text to draw
        $title = 'Easy ID';

        // Replace path by your own font path
        $font = ROOT_DIRECTORY . "/public/fonts/arial.ttf";

        // Add some shadow to the text
        imagettftext($im, 25, 0, 10, 35, $black, $font, $title);

        $currentStringPositionX = 10;
        $currentStringPositionY = 75;
        foreach($this->getKeySet() as $setItem) {
            $text = $setItem->id . " " . $setItem->key;
            imagettftext($im, 18, 0, $currentStringPositionX, $currentStringPositionY, $black, $font, $text);
            $currentStringPositionY += 25;
            if($currentStringPositionY >= 800) {
                $currentStringPositionY = 75;
                $currentStringPositionX += 200;
            }
        }
        $date = new DateTime();
        $savePath = ROOT_DIRECTORY . "/temp/" . strtolower("ID_" . $this->keySetId . "_" . $date->getTimestamp())  . ".png";
        imagepng($im, $savePath, 0, null);
        imagedestroy($im);

        return $savePath;
    }

    public function getKeySet() {
        return $this->keySet;
    }

    public function getKeySetId() {
        return $this->keySetId;
    }

    public function getActiveKeySet() {
        $easyIdId = $this->getUserKeySetId();
        if($easyIdId) {
            $sql = "SELECT `keySetId`, `keyHash`
                FROM `easyidkeyset`
                WHERE `active` = TRUE
                AND `easyIdId` = ?
                ORDER BY RAND()
                LIMIT 1;";

            $keySet = $this->db->query($sql, array($easyIdId));
            $keySet = (is_array($keySet) && count($keySet) === 1) ? array_shift($keySet) : null;

            if(is_null($keySet)) {
                return null;
            }

            $keySet->keySetId = $this->formatId($keySet->keySetId);

            return $keySet;
        }
        return null;
    }

    public function getUserKeySetId() {
        $sql = "SELECT id FROM `easyid`
                WHERE userId = ?";
        $id = $this->db->query($sql, array($this->userId));
        $id = $this->getSingleResult($id);
        if($id) {
            return (int)$id->id;
        }
        return null;
    }

    public function validateKeySet($id, $key) {
        $sql = "SELECT *
                FROM `easyidkeyset`
                WHERE `keySetId` = ?
                LIMIT 1";

        $keySet = $this->db->query($sql, array((int)$id));

        if($keySet) {
            $keySet = (is_array($keySet) && count($keySet) === 1) ? array_shift($keySet) : null;
            if(is_null($keySet)) {
                return null;
            }
            if(Hasher::valueIsValid($key, $keySet->keyHash)) {
                return $keySet;
            }
        }
        return null;
    }

    /**
     * Updates the given keyset to be inactive
     *
     * @param $keySet
     * @return bool|PDOStatement
     */
    public function deactivateKeySet($keySet) {
        $sql = "UPDATE `easyidkeyset`
                SET `active` = FALSE
                WHERE `id` = ?";

        return $this->db->query($sql, array($keySet->id));
    }
}