<html>
    <head>
        <title><?php echo $data['site_title'];?></title>
        <meta charset="utf-8">
        <meta name="description" content="The bank for all that wnants it safe">
        <meta name="author" content="Hanning Høegh, Steffen Mortensen & Morten Olgenkær">
        <link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT;?>/public/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT;?>/public/css/bootstrap-theme.css">
        <link rel="icon" type="image/png" href="../public/img/hackersBanskLogoIcon.png"/>
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"></button>
                    <a class="navbar-brand active" href="/">Hackers Bank</a>
                </div>
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!--<li><a href="/about">About</a></li>-->
                        <?php if(!$data['loggedIn']) { ?>
                            <li><a href="/login">Login</a></li>
                            <li><a href="/user/signup">Sign up</a></li>
                        <?php } else { ?>
                            <li><a href="/overview">Overview</a></li>
                        <?php } ?>
                    </ul>
               </div>
            </div>
        </div>