        <div class="container">
            <div class="starter-template">
                <form class="form-horizontal" action="login/login" method="POST">
                    <fieldset>
                        <legend>Login</legend>
                        <div class="control-group">
                            <label class="control-label" for="textinput">Email</label>
                            <div class="controls">
                                <input id="email" name="email" type="text" placeholder="email" class="input-xlarge">
                                <p class="help-block">Type in your email</p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="passwordinput">Password Input</label>
                            <div class="controls">
                                <input id="password" name="password" type="password" placeholder="password" class="input-xlarge">
                                <p class="help-block">Type in your password</p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="singlebutton">Click to login</label>
                            <div class="controls">
                                <input type="submit" value="Login" id="singlebutton" name="singlebutton" class="btn btn-primary">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div><!-- /.container -->