        <div class="container">
            <div class="starter-template">
                <!-- ------------------------------------------- -->
                
                <div class="container" id="wrap">
                    <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <form action="/user/Register" method="post" accept-charset="utf-8" class="form form-horizontal" role="form">   <legend>Sign Up</legend>
                                <h4>It's secure!</h4>
                                <div class="row">
                                    <div class="col-xs-6 col-md-6">
                                        <input type="text" name="firstname" value="" class="form-control input-lg" placeholder="First Name" required  />                        </div>
                                    <div class="col-xs-6 col-md-6">
                                        <input type="text" name="lastname" value="" class="form-control input-lg" placeholder="Last Name" required  />                        </div>
                                </div>
                                <input type="text" name="email" value="" class="form-control input-lg" placeholder="Your Email" required  />
                                <input type="password" name="password" value="" class="form-control input-lg" placeholder="Password" required  />
                                <input type="password" name="confirm_password" value="" class="form-control input-lg" placeholder="Confirm Password" required />                    <label>Birth Date</label>                    <div class="row">
                                    <div class="col-xs-4 col-md-4">
                                        <select name="month" class = "form-control input-lg">
                                            <?php
                                                $months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
                                                for ($x=1; $x<=12; $x++)
                                                {
                                                    echo '<option value='.$x.'>'.$months[$x-1].'</option>';
                                                } 
                                            ?>                                     
                                        </select>                        
                                    </div>
                                    <div class="col-xs-4 col-md-4">
                                        <select name="day" class = "form-control input-lg">
                                            <?php
                                                for ($x=1; $x<=31; $x++)
                                                {
                                                    echo '<option value='.$x.'>'.$x.'</option>';
                                                } 
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-4 col-md-4">
                                        <select name="year" class = "form-control input-lg">
                                            <?php
                                                for ($x=1935; $x<=1996; $x++)
                                                {
                                                    echo '<option value='.$x.'>'.$x.'</option>';
                                                } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <label>Gender : </label>     
                                <label class="radio-inline">
                                        <input type="radio" name="gender" value="M" id=male required />Male
                                        </br>
                                        <input type="radio" name="gender" value="F" id=female required />Female
                                </label>
                                <br />
                          <span class="help-block">By clicking Create my account, you agree to our Terms and that you have read our Data Use Policy, including our Cookie Use. Click <a>here</a> fo read the terms.</span>
                                <button class="btn btn-lg btn-primary btn-block signup-btn" type="submit">
                                    Create my account</button>
                        </form>          
                      </div>
            </div>            
            </div>
          </div>
                <!-- ------------------------------------------- -->
            </div>