<?php

class IndexController extends Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $content_view = new View();
        $content_view->render("frontpage");
    }
}