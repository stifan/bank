<?php

class View {
    private $data = array();
    private $render = false;

    public function __construct() {

        $this->data['site_title'] = 'Hackers Bank';
        $this->data['loggedIn'] = false;

        $session = new Session();
        if(!is_null($session->getLoggedInUser())) {
            $this->data['loggedIn'] = true;
        }
    }

    public function assign($var = '', $val) {
        if($var == '') {
            $this->data = $val;
        } else {
            $this->data[$var] = $val;
        }
    }

    public function render($view, $direct_output = true) {
        if(substr($view, -4) == ".php") {
            $file = $view;
        } else {
            $file = ROOT . DS . 'views' . DS . strtolower($view) . ".php";
        }
        if(file_exists($file)) {
            /**
             * Trigger to include file when model is destroyed
             */
            $this->render = $file;
        } else {
            return "View file does not exist.";
        }

        // Turn output buffering on, capturing all output
        if($direct_output !== true) {
            ob_start();
        }

        // parse data vars into local vars
        $data = $this->data;

        include ROOT_DIRECTORY.'/views/header.php';
        // get template
        include($this->render);

        include ROOT_DIRECTORY.'/views/footer.php';

        // Get the contets of the buffer and return it
        if($direct_output !== true) {
            return ob_get_clean();
        }
    }
}