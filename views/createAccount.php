<div class="container">
    <div class="paymentform">
        <div class="row">
            <div class='col-md-4'></div>
            <div class='col-md-4'><h1>Create Account</h1></div>
            <div class='col-md-4'></div>
        </div>

        <div class='row'>
        <div class='col-md-4'></div>
        <div class='col-md-4 greybackground'>

          <form action="/account/createAccount/" class="require-validation" id="transfer-form" method="post">
            <div class='form-row'>
                <div class='col-xs-12 form-group required'>
                    <label class='control-label'>Name</label>
                    <input name="name" class='form-control' size='4' type='text'>
                    <input type="hidden" name="token" value="<?php echo $data['token']; ?>">
                </div>
            </div>
            <div class='form-row'>
              <div class='col-md-12 form-group'>
                <button class='form-control btn btn-success submit-button total' type='submit'>Create</button>
              </div>
            </div>
          </form>
        </div>
        <div class='col-md-4'></div>
    </div>
    </div>
</div>