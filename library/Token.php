<?php

class Token {
    /**
     * Generate a random string of 32 chars using openssl
     * and set the token in the session.
     *
     * @return string
     */
    public static function generate() {
        $session = new Session();
        $token = base64_encode(openssl_random_pseudo_bytes(32));
        $session->set("token", $token);
        return $token;
    }

    /**
     * Validate a given token and unset it if successfull
     * @param $token
     * @return bool
     */
    public static function check($token) {
        $session = new Session();
        if($token === $session->get("token")) {
            $session->sessionUnset("token");
            return true;
        }
        return false;
    }
} 