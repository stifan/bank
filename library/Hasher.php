<?php
/**
 * Created by PhpStorm.
 * User: steffen
 * Date: 07-01-14
 * Time: 13:02
 */

class Hasher {

    /**
     * Creates a hashed result of a given value using
     *
     * @param $val
     * @return null|string
     */
    public static function hashValue($val) {
        if($val) {
            $salt = self::generateRandomSalt(22);
            return crypt($val, $salt);
        }

        return null;
    }

    /**
     * Validates a given value against a hash
     *
     * @param $val
     * @param $hash
     * @return bool
     */
    public static function valueIsValid($val, $hash) {
        if ($hash === crypt($val, $hash)) {
            return true;
        }
        return false;
    }

    /**
     * Generates a string of random characters in a given length
     *
     * Use blowfish to make it harder to brute force
     * using openssl
     * @param $length
     * @return string
     */
    private static function generateRandomSalt($length) {
        return "$2y$07$".bin2hex(openssl_random_pseudo_bytes($length));
    }
} 