<?php

class TransferController extends Controller {
    public function __construct() {
        parent::__construct();

        $this->validateUser();
    }

    public function index() {
        $accountModel = new AccountModel();
        // Generate and assign request token for form
        $this->view->assign("token", Token::generate());

        $accounts = $accountModel->getUserAccounts($this->session->getLoggedInUser());
        $this->view->assign("accounts", $accounts);
        $this->view->render("transfer");
    }
    
    public function transfer() {
        if(!Token::check($_POST['token'])) {
            die("Invalid request token, you BASTARD!");
        }

        $amount = Validator::isNumeric($_POST['amount']);
        $senderAccount = Validator::isNumeric($_POST['senderAccount']);
        $receiverAccount = Validator::isNumeric($_POST['receiverAccount']);
        $comment = Validator::sanitizeInput($_POST['comment']);
        $note = Validator::sanitizeInput($_POST['note']);

        // Cancel transaction if the amount is less than zero
        if((float)$amount < 0) {
            $this->redirect('transfer/');
            exit;
        }

        if(is_null($amount) || is_null($senderAccount) || is_null($receiverAccount) || is_null($comment) ){
            $this->redirect('transfer/');
            exit;
        }

        // check if accounts are valid
        $accountModel = new AccountModel();
        $senderAccount = $accountModel->getAccount($senderAccount);
        $receiverAccount = $accountModel->getAccount($receiverAccount);

        if(is_null($senderAccount) || is_null($receiverAccount)) {
            // One of the accounts were invalid
            $this->redirect('transfer/');
            exit;
        }

        // Make sure that the sender account has enough money
        if((float)$senderAccount->balance < (float)$amount) {
            // Sender account does not have enough money
            $this->redirect('transfer/');
            exit;
        }

        $transferModel = new TransferModel();
        $return = $transferModel->transfer($amount, $note, (int)$senderAccount->id, (int)$receiverAccount->id, $comment);
        if($return) {
            // Transfer went through redirect to account page
            $this->redirect("/account/show/" . $senderAccount->id);
        } else {
            // transfer failed redirect back transfer
            $this->redirect("/transfer/");
        }
    }
}